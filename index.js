'use strict';

module.exports = {
    platforms: {
        applepodcasts: {
          key: 'applepodcasts',
          name: 'Apple Podcasts',
          url: 'podcasts.apple.com',
          wideThumb: false,
          urlMatch: /(?:https?:\/\/)?(?:www\.)?((podcasts\.apple\.com\/)([A-Za-z]{2})?(\/podcast\/))(([A-Za-z0-9-]{1,})?(\/)?((id)([0-9]{10}))+)/,
          extract: undefined
        },
        askdifferent: {
            key: 'askdifferent',
            name: 'Ask Different',
            url: 'apple.stackexchange.com',
            hex: '#74a5d2',
            wideThumb: true,
            urlMatch: /(?:https?:\/\/)?(?:www\.)?(?:apple\.stackexchange\.com\/(questions)?)([A-Za-z0-9-_]{1,})?(\/)?([A-Za-z0-9-_]{1,}\/)?([A-Za-z0-9-_]{1,})?/,
            extract: (link) => link.split('apple.stackexchange.com/')[1].split('/')[1]
        },
        askfm: {
            key: 'askfm',
            name: 'ASKfm',
            url: 'ask.fm',
            hex: '#C6304A',
            wideThumb: true,
            urlMatch: /http(?:s)?:\/\/(?:www\.)?ask\.fm\/([a-zA-Z0-9_]+)/,
            extract: (link) => link.split('ask.fm/')[1].split('/')[0]
        },
        askubuntu: {
            key: 'askubuntu',
            name: 'Ask Ubuntu',
            url: 'askubuntu.com',
            hex: '#dc461d',
            wideThumb: true,
            urlMatch: /(?:https?:\/\/)?(?:www\.)?(?:askubuntu\.com\/(questions)?)([A-Za-z0-9-_]{1,})?(\/)?([A-Za-z0-9-_]{1,}\/)?([A-Za-z0-9-_]{1,})?/,
            extract: (link) => link.split('askubuntu.com/')[1].split('/')[1]
        },
        bitbucket: {
            key: 'bitbucket',
            name: 'Bitbucket',
            url: 'bitbucket.org',
            hex: '#205081',
            wideThumb: true,
            urlMatch: /http(?:s)?:\/\/(?:www\.)?bitbucket\.org\/([a-zA-Z0-9_]+)/,
            extract: undefined
        },
        discord: {
            key: 'discord',
            name: 'Discord',
            url: 'discord.gg',
            hex: '#7289da',
            wideThumb: false,
            urlMatch: /^(?:https?:\/\/)?(?:www\.)?(?:discord\.gg\/|discordapp\.com\/invite)(?:\S+)?$/,
            extract: (link) => (link.includes('discordapp.com/invite') ? link.split('discordapp.com/invite')[1].split('/')[1] : link.split('discord.gg/')[1].split('/')[0])
        },
        facebook: {
            key: 'facebook',
            name: 'Facebook',
            url: 'facebook.com',
            hex: '#4267b2',
            wideThumb: false,
            urlMatch: /http(?:s)?:\/\/(?:www\.)?facebook\.com\/([a-zA-Z0-9_]+)/,
            extract: undefined
        },
        floatplane: {
            key: 'floatplane',
            name: 'Floatplane',
            url: 'floatplane.com',
            hex: '#08AAF2',
            wideThumb: false,
            urlMatch: /http(?:s)?:\/\/(?:www\.)?floatplane\.com\/channel\/([a-zA-Z0-9_]+)/,
            extract: (link) => link.split('floatplane.com/video/')[1].split('/')[0]
        },
        gamedevelopment: {
            key: 'gamedevelopment',
            name: 'Game Development',
            url: 'gamedev.stackexchange.com',
            hex: '#fffeee',
            wideThumb: true,
            urlMatch: /(?:https?:\/\/)?(?:www\.)?(?:gamedev\.stackexchange\.com\/(questions)?)([A-Za-z0-9-_]{1,})?(\/)?([A-Za-z0-9-_]{1,}\/)?([A-Za-z0-9-_]{1,})?/,
            extract: extract: (link) => link.split('gamedev.stackexchange.com/')[1].split('/')[1]    
        },
        github: {
            key: 'github',
            name: 'Github',
            url: 'github.com',
            hex: '#24292e',
            wideThumb: true,
            urlMatch: /http(?:s)?:\/\/(?:www\.)?(?:gist\.)?github\.com\/([a-zA-Z0-9_]+)/,
            extract: undefined
        },
        gitlab: {
            key: 'gitlab',
            name: 'Gitlab',
            url: 'gitlab.com',
            hex: '#E24329',
            wideThumb: true,
            urlMatch: /http(?:s)?:\/\/(?:www\.)?(?:gist\.)?gitlab\.com\/([a-zA-Z0-9_]+)/,
            extract: undefined
        },
        geographicinformationsystems: {
            key: 'geographicinformationsystems',
            name: 'Geographic Information Systems',
            url: 'gis.stackexchange.com',
            hex: '#f5f5ed',
            wideThumb: true,
            urlMatch: /(?:https?:\/\/)?(?:www\.)?(?:gis\.stackexchange\.com\/(questions)?)([A-Za-z0-9-_]{1,})?(\/)?([A-Za-z0-9-_]{1,}\/)?([A-Za-z0-9-_]{1,})?/,
            extract: extract: (link) => link.split('gis.stackexchange.com/')[1].split('/')[1] 
        },
        instagram: {
            key: 'instagram',
            name: 'Instagram',
            url: 'instagram.com',
            hex: '#e33567',
            wideThumb: false,
            urlMatch: /http(?:s)?:\/\/(?:www\.)?instagram\.com\/([a-zA-Z0-9_]+)/,
            extract: undefined
        },
        mixer: {
            key: 'mixer',
            name: 'Mixer',
            url: 'mixer.com',
            hex: '#29bbed',
            wideThumb: true,
            urlMatch: /http(?:s)?:\/\/(?:www\.)?mixer\.com\/([a-zA-Z0-9_]+)/,
            extract: (link) => link.split('mixer.com/')[1].split('/')[0]
        },
        myspace: {
            key: 'myspace',
            name: 'myspace',
            url: 'myspace.com',
            hex: '#231F1E',
            wideThumb: true,
            urlMatch: /http(?:s)?:\/\/(?:www\.)?myspace\.com\/([a-zA-Z0-9_]+)/,
            extract: (link) => link.split('myspace.com/')[1].split('/')[0]
        },
        notify: {
            key: 'notify',
            name: 'Notify',
            url: 'notify.me',
            hex: '#1bb9dc',
            wideThumb: false,
            urlMatch: /http(?:s)?:\/\/(?:blog\.)?notify\.me\/([a-zA-Z0-9_]+)/,
            extract: undefined
        },
        notion: {
            key: 'notion',
            name: 'Notion',
            url: 'notion.so',
            hex: '#060605',
            wideThumb: false,
            urlMatch: /(?:https?:\/\/)?(?:www\.)?(?:notion\.so\/)([A-Za-z0-9-_]{1,})(\/)?([A-Za-z0-9-_]{1,})?([A-Za-z0-9]{30})?/,
            extract: undefined
        },
        patreon: {
            key: 'patreon',
            name: 'Patreon',
            url: 'patreon.com',
            hex: '#f96854',
            wideThumb: false,
            urlMatch: /http(?:s)?:\/\/(?:www\.)?patreon\.com\/([a-zA-Z0-9_]+)/,
            extract: undefined
        },
        pocketcasts: {
            key: 'pocketcasts',
            name: 'Pocket Casts',
            url: 'pocketcasts.com',
            hex: '#f43e37',
            wideThumb: false,
            urlMatch: /^(?:https?:\/\/)?(?:www\.)?(?:pca\.st\/|play\.pocketcasts\.com\/)(((web\/podcasts\/share\?id=)?([a-f0-9\\-]{36}))|([A-Za-z0-9-_]{1,4}))/,
            extract: undefined
        },
        periscope: {
            key: 'periscope',
            name: 'Periscope',
            url: 'pscp.tv',
            hex: '#40A4C4',
            wideThumb: true,
            urlMatch: /http(?:s)?:\/\/(?:www\.)?pscp\.tv\/([a-zA-Z0-9_]+)/,
            extract: (link) => link.split('pscp.tv/')[1].split('/')[0]
        },
        pinterest: {
            key: 'pinterest',
            name: 'Pinterest',
            url: 'pinterest.com',
            hex: '#cb2027',
            wideThumb: true,
            urlMatch: /http(?:s)?:\/\/(?:www\.)?pinterest\.com\/([a-zA-Z0-9_]+)/,
            extract: (link) => link.split('pinterest.com/')[1].split('/')[0]
        },
        prowebmasters: {
            key: 'prowebmasters',
            name: 'Pro Webmasters',
            url: 'webmasters.stackexchange.com',
            hex: '#69d1f9',
            urlMatch: /(?:https?:\/\/)?(?:www\.)?(?:webmasters\.stackexchange\.com\/(questions)?)([A-Za-z0-9-_]{1,})?(\/)?([A-Za-z0-9-_]{1,}\/)?([A-Za-z0-9-_]{1,})?/,
            extract: extract: (link) => link.split('webmasters.stackexchange.com/')[1].split('/')[1]
        },
        reddit: {
            key: 'reddit',
            name: 'Reddit',
            url: 'reddit.com',
            hex: '#4267b2',
            wideThumb: false,
            urlMatch: /http(?:s)?:\/\/(?:www\.)?reddit\.com\/([a-zA-Z0-9_]+)/,
            extract: undefined
        },
        serverfault: {
            key: 'serverfault',
            name: 'ServerFault',
            url: 'serverfault.com',
            hex: '#9c1723',
            wideThump: true,
            urlMatch: /(?:https?:\/\/)?(?:www\.)?(?:serverfault\.com\/(questions)?)([A-Za-z0-9-_]{1,})?(\/)?([A-Za-z0-9-_]{1,}\/)?([A-Za-z0-9-_]{1,})?/,
            extract: (link) => link.split('serverfault.com/')[1].split('/')[1]
        },
        snapchat: {
            key: 'snapchat',
            name: 'snapchat',
            url: 'snapchat.com',
            hex: '#FFFC00',
            wideThumb: true,
            urlMatch: /http(?:s)?:\/\/(?:www\.)?snapchat\.com\/add\/([a-zA-Z0-9_]+)/,
            extract: (link) => link.split('snapchat.com/')[1].split('/')[0]
        },
        soundcloud: {
            key: 'soundcloud',
            name: 'SoundCloud',
            url: 'soundcloud.com',
            hex: '#ff8800',
            wideThumb: false,
            urlMatch: /((https:\/\/)|(http:\/\/)|(www.)|(m\.)|(\s))+(soundcloud.com\/)+[a-zA-Z0-9\-\.]+(\/)+[a-zA-Z0-9\-\.]+/,
            extract: undefined
        },
        softwareengineering: {
            key: 'softwareengineering',
            name: 'Software Engineering',
            url: 'softwareengineering.stackexchange.com',
            hex: '#316ab3',
            wideThumb: true,
            urlMatch: /(?:https?:\/\/)?(?:www\.)?(?:softwareengineering\.stackexchange.c\.com\/(questions)?)([A-Za-z0-9-_]{1,})?(\/)?([A-Za-z0-9-_]{1,}\/)?([A-Za-z0-9-_]{1,})?/,
            extract: (link) => link.split('softwareengineering.stackexchange.com/')[1].split('/')[1]
        },
        spotify: {
            key: 'spotify',
            name: 'Spotify',
            url: 'spotify.com',
            hex: '#1DB954',
            wideThumb: false,
            urlMatch: /http(?:s)?:\/\/(?:www\.)?open.spotify\.com\/([a-zA-Z0-9_]+)/,
            extract: undefined
        },
        stackoverflow: {
            key: 'stackoverflow',
            name: 'StackOverflow',
            url: 'stackoverflow.com',
            hex: '#f48024',
            wideThumb: true,
            urlMatch: /http(?:s)?:\/\/(?:www\.)?stackoverflow\.com\/([a-zA-Z0-9_]+)/,
            extract: (link) => link.split('stackoverflow.com/')[1].split('/')[1]
        },
        superuser: {
            key: 'superuser',
            name: 'SuperUser',
            url: 'superuser.com',
            hex: '#38a1ce',
            wideThumb: true,
            urlMatch: /(?:https?:\/\/)?(?:www\.)?(?:superuser\.com\/(questions)?)([A-Za-z0-9-_]{1,})?(\/)?([A-Za-z0-9-_]{1,}\/)?([A-Za-z0-9-_]{1,})?/,
            extract: (link) => link.split('superuser.com/')[1].split('/')[1]
        },
        tex: {
            key: 'tex',
            name: 'SuperUser',
            url: 'tex.com',
            hex: '#9f393c',
            wideThumb: true,
            urlMatch: /(?:https?:\/\/)?(?:www\.)?(?:tex\.stackexchange\.com\/(questions)?)([A-Za-z0-9-_]{1,})?(\/)?([A-Za-z0-9-_]{1,}\/)?([A-Za-z0-9-_]{1,})?/,
            extract: (link) => link.split('tex.stackexchange.com/')[1].split('/')[1]
        },
        tiktok: {
            key: 'tiktok',
            name: 'TikTok',
            url: 'tiktok.com',
            hex: '#40A4C4',
            wideThumb: true,
            urlMatch: /http(?:s)?:\/\/(?:www\.)?tiktok\.com\/share\/user\/([0-9]+)/,
            extract: (link) => link.split('tiktok.com/embed/')[1].split('/')[0]
        },
        twitch: {
            key: 'twitch',
            name: 'Twitch',
            url: 'twitch.tv',
            hex: '#6441A4',
            wideThumb: true,
            urlMatch: /http(?:s)?:\/\/(?:www\.)?twitch\.tv\/([a-zA-Z0-9_]+)/,
            extract: (link) => link.split('twitch.tv/')[1].split('/')[0]
        },
        twitter: {
            key: 'twitter',
            name: 'Twitter',
            url: 'twitter.com',
            hex: '#00aced',
            wideThumb: false,
            urlMatch: /http(?:s)?:\/\/(?:www\.)?twitter\.com\/([a-zA-Z0-9_]+)/,
            extract: undefined
        },
        tumblr: {
            key: 'tumblr',
            name: 'Tumblr',
            url: 'tumblr.com',
            hex: '#34526f',
            wideThumb: false,
            urlMatch: /http(?:s)?:\/\/(?:www\.)?tumblr\.com\/([a-zA-Z0-9_]+)/,
            extract: (link) => link.split('tumblr.com/')[1].split('/')[1]
        },
        unixandlinux: {
            key: 'unixandlinux',
            name: 'Unix & Linux',
            url: 'unix.stackexchange.com',
            hex: '#155078',
            wideThumb: true,
            urlMatch: /(?:https?:\/\/)?(?:www\.)?(?:unix\.stackexchange\.com\/(questions)?)([A-Za-z0-9-_]{1,})?(\/)?([A-Za-z0-9-_]{1,}\/)?([A-Za-z0-9-_]{1,})?/,
            extract: (link) => link.split('unix.stackexchange.com/')[1].split('/')[1]
        },
        youtube: {
            key: 'youtube',
            name: 'YouTube',
            url: 'youtube.com',
            hex: '#ff0000',
            wideThumb: true,
            urlMatch: /^(?:https?:\/\/)?(?:m\.|www\.)?(?:youtu\.be\/|youtube\.com\/(?:embed\/|v\/|watch\?v=|watch\?.+&v=))((\w|-){11})(?:\S+)?$/,
            extract: (link) => link.match(/^.*(youtu.be\/|v\/|e\/|u\/\w+\/|embed\/|v=)([^#\&\?]*).*/).filter(result => result.length === 11)[0]
        },
        webapplications: {
            key: 'webapplications',
            name: 'Web Applications',
            url: 'webapps.stackexchange.com',
            hex: '#038ad6',
            urlMatch: /(?:https?:\/\/)?(?:www\.)?(?:webapps\.stackexchange\.com\/(questions)?)([A-Za-z0-9-_]{1,})?(\/)?([A-Za-z0-9-_]{1,}\/)?([A-Za-z0-9-_]{1,})?/,
            extract: extract: (link) => link.split('webapps.stackexchange.com/')[1].split('/')[1]
        },
        wordpressdev: {
            key: 'wordpressdev',
            name: 'WordPress development',
            url: 'wordpress.stackexchange.com',
            hex: '#363b42',
            urlMatch: /(?:https?:\/\/)?(?:www\.)?(?:wordpress\.stackexchange\.com\/(questions)?)([A-Za-z0-9-_]{1,})?(\/)?([A-Za-z0-9-_]{1,}\/)?([A-Za-z0-9-_]{1,})?/,
            extract: extract: (link) => link.split('wordpress.stackexchange.com/')[1].split('/')[1]
        }
    }
};

// to test stuff you're adding comment out the export above, run this using "node platforms.js" and console log instead
// console.log(platforms['twitch'].extract('https://twitch.tv/jamiepinelive'));
